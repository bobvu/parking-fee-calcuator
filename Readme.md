# Hung - Simple Rate Calculator API for Car Park

This API is a simple implementation which calculate the parking fee using on the provided rates. Inputs are `entryTime` and `exitTime` and the outcome is a `FinalRate` object which contains `RatingType, Name, and Price`.

## Demo

You can test youself by giving different pair of inputs at [https://codingchallengsapi.com.au](https://codingchallengesapi.itmb.com.au/api/parking?entryTime=2020-4-24%2018:50&exitTime=2020-04-25%206:50)

## The Approach

When it comes, I believe it is more than just an API with simple calculating algorithm coding in front of me. My plan is to provide a end-to-end solution for the API, and it will cover most of the software development life cycle. Sounds good? Let's start.

First of all, assuming those requirements have been refined and confirmed with the business analyst in a sprint planning meeting or so, it will be documented as UAC (user acceptance criteria) somewhere in JIRA or Trello, and the content will look like “As a customer, I should be able to xxx...” at the end of day. In this particular user case, we have different strategies to calculate the rate, so natuarlly the idea of using Strategy design patter comes to my mind. By applying this pattern, each rule can be tested easily. Using `Switch...Case`is a bad best practice from my perspective. With the fact that we only have 4 rules here (EarlyBrid, Night, Weekend, Hourly) and they are not very complicated, I thought that I can put them all in one big fat `CalculatingService`, in the right order of course, and the problem is solved. Then it came to my mind _what if we have more rules in the future like hundred more rules_. Thinking of managing the order of those rules gives me headeach already, not to mention by adding a new rule, it also means that you have to come back and update the `CalculatingService` again. Soon the `CalculatingService` will get hairy and ugly. No, I don't wanna go that path!!! I want to design a more scalable solution that even when you want to add thoudsands more rules, you still can survive. You can just simply add a new rule (based on a give format), putting into one given folder then the `engine` will automatically pick it up to calculate the rate for you, and you don't need to care about the order.

Secondly, architecture wise, I will apply Domain Driven Design (DDD) pattern with CQRS using MediatR, along with SOLID principles, and TDD as a must. By using DDD, we can serve each domain/bounded context as a place to store core business logics which is the centre of our application and it's independent to any technologies we use. By using CQRS & Mediatr, we can separate the commands and queries and apply the concept of thin API. Yes, thin but clean. Now, the jobs that the API normally does like throwing exceptions or handling validation or anything like that are moved to the Services layer. All the API needs to do now is to send a command/query and the Mediatr will send it to the proper handler (For example the `Calculate.Query` will automatically be handled by the `Calculate.Handler.Handle` in the file 'Services/Caculate.cs'). Other design patterns used:

- SOLID design principles, TDD - Strategy, Dependency Injection...
- Validation using `FluentValidation.AspNetCore`
- Handle Exceptions using middleareware `ErrorHandlingMiddleware.cs`
- New extension called `RegisterAllAssignableTypes` was created to register all the rules that inherits the `BaseRule`abstract class into the `serviceProvider`.

Third, deploy using `Docker` and `CI/CD with Bitbucket Pipeline`. No matter how awesome a product is, it can’t survive if it fails to provide a always-online service and catch up the business capacity expansion. Containerised application is definitely an option especially when it is working with tools like Kubernetes, stability and scalability can both be achieved and controlled with a very low risk.

The demo is available at: [https://codingchallengsapi.com.au](https://codingchallengesapi.itmb.com.au/api/parking?entryTime=2020-4-24%2018:50&exitTime=2020-04-25%206:50)

You can view the build/deploy results at pipelines: [https://bitbucket.org/bobvu/parking-fee-calcuator/addon/pipelines/home#!/](https://bitbucket.org/bobvu/parking-fee-calcuator/addon/pipelines/home#!/)

## Setup and Run

**Prerequisites:**

- .Net Core 2.1
- VSCode or Visual Studio Code.
- Postman or any browser

To run the API, clone this repository into your local machine, cd into the `parking-fee-calculator` folder, open `terminal`on Mac or `cmd` on Windows at that folder and run:

```cs
dotnet restore
cd ParkingAPI/
dotnet run
```

To run unitets, cd to `Tests`folder and run

```cs
dotnet test
```

The back-end server will run on port `5000` by default.

- Open [http://localhost:5000](http://localhost:5000) to view it in the browser.
- If you are using postman, you can easily test the engine by sending GET request with different pair of inputs to test . (e.g `https://localhost:5001/api/parking?entryTime=2020-4-24%2018:50&exitTime=2020-04-25%206:50`)

Have fun!!!
