﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace ParkingAPI.Extensions
{
    public static class RegisterAllAssignableTypes
    {
        public static void RegisterAllAssignableType<T>(this IServiceCollection services, string assemblyName)
        {
            var assembly = AppDomain.CurrentDomain.Load(assemblyName);
            var types = assembly.GetTypes().Where(p => typeof(T).IsAssignableFrom(p)).ToArray();

            var addTransientMethod = typeof(ServiceCollectionServiceExtensions).GetMethods().FirstOrDefault(m =>
               m.Name == "AddTransient" &&
               m.IsGenericMethod == true &&
              m.GetGenericArguments().Count() == 2);

            foreach (var type in types)
            {
                if (type.IsAbstract)
                    continue;

                var method = addTransientMethod.MakeGenericMethod(new[] { typeof(T), type });
                method.Invoke(services, new[] { services });
            }
        }

        public static T ResolveByName<T>(this IServiceProvider serviceProvider, string typeName)
        {
            var allRegisteredTypes = serviceProvider.GetRequiredService<IEnumerable<T>>();
            var resolvedService = allRegisteredTypes.FirstOrDefault(p => p.GetType().FullName.Contains(typeName));

            if (resolvedService != null)
            {
                return resolvedService;
            }
            else
            {
                throw new Exception($"{typeName} type not found.");
            }
        }

        public static IEnumerable<T> GetAll<T>(this IServiceProvider serviceProvider)
        {
            var instances = serviceProvider.GetRequiredService<IEnumerable<T>>();

            if (instances.Any())
            {
                return instances;
            }
            else
            {
                throw new Exception("Type type not found.");
            }
        }
    }
}

