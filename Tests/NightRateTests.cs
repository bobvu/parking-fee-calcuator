﻿using System;
using Xunit;
using Domains.Parking;
namespace Tests
{
    public class NightRateTests : BaseTest
    {
        [Fact]
        public void When_a_car_enters_at6pmOrAfter_exits_before_8am_nextMorning_qualify_NightRate()
        {
            var startTime = new DateTime(2020, 04, 23, 18, 0, 0);
            var endTime = new DateTime(2020, 04, 24, 7, 15, 0);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(NightRate, rate.Price);
            Assert.Equal(RatingType.FlatRate.ToString(), rate.Type);
            Assert.Equal("Night Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_enters_before6pm_disqaulifiesNightRate()
        {
            var startTime = new DateTime(2020, 04, 23, 17, 20, 0);
            var endTime = new DateTime(2020, 04, 24, 18, 0, 0);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Night Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_exiting8amOrAfter_disqualify_NightRate()
        {
            var startTime = new DateTime(2020, 04, 23, 18, 0, 0);
            var endTime = new DateTime(2020, 04, 24, 8, 1, 0);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Night Rate", rate.Name);
        }

     
    }
}
