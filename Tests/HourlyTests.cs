﻿using System;
using Domains.Parking;
using Services;
using Xunit;

namespace Tests
{
    public class HourlyTests : BaseTest
    {
       

        [Fact]
        public void When_a_car_exits_in_30mins_return_hourly_rate()
        {
            var startTime = new DateTime(2020, 04, 23, 4, 0, 0);
            var endTime = startTime.AddMinutes(30);
            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(HourlyRate, rate.Price);
            Assert.Equal(RatingType.HourlyRate.ToString(), rate.Type);
            Assert.Equal("Standard Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_exits_in_2hours_return_hourly_rate()
        {
            var startTime = new DateTime(2020, 04, 23, 4, 0, 0);
            var endTime = startTime.AddHours(2);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(HourlyRate * 2, rate.Price);
            Assert.Equal(RatingType.HourlyRate.ToString(), rate.Type);
            Assert.Equal("Standard Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_exits_in_morethan_2hours_return_hourly_rate()
        {
            var startTime = new DateTime(2020, 04, 23, 4, 0, 0);
            var endTime = startTime.AddHours(2).AddMinutes(2);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(HourlyRate * 3, rate.Price);
            Assert.Equal(RatingType.HourlyRate.ToString(), rate.Type);
            Assert.Equal("Standard Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_exits_in_morethan_3hours_return_flat_rate()
        {
            var startTime = new DateTime(2020, 04, 23, 4, 0, 0);
            var endTime = startTime.AddHours(3).AddMinutes(3);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(HourlyRate * 4, rate.Price);
            Assert.Equal(RatingType.HourlyRate.ToString(), rate.Type);
            Assert.Equal("Standard Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_exits_in_morethan_3hours_but_lessthan_24h_return_flat_rate()
        {
            var startTime = new DateTime(2020, 04, 23, 0, 0, 0);
            var endTime = startTime.AddHours(23).AddMinutes(59).AddSeconds(59);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(DayRate , rate.Price);
            Assert.Equal(RatingType.HourlyRate.ToString(), rate.Type);
            Assert.Equal("Standard Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_exits_in_morethan_3Hours_over_three_days_3DayRate()
        {
            var startTime = new DateTime(2020, 04, 23, 16, 0, 0);
            var endTime = startTime.AddHours(36);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(DayRate * 3, rate.Price);
            Assert.Equal(RatingType.HourlyRate.ToString(), rate.Type);
            Assert.Equal("Standard Rate", rate.Name);
        }

    }
}
