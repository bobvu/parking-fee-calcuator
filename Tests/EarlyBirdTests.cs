﻿using System;
using Xunit;
namespace Tests
{
    public class EarlyBirdTests : BaseTest
    {
        [Fact]
        public void When_a_car_enter_at_805am_exits_at_1100pm_is_EarlyBird_applies()
        {
            var startTime = new DateTime(2020, 04, 23, 6, 5, 0);
            var endTime = new DateTime(2020, 04, 23, 23, 0, 0);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(EarlyBirdRate, rate.Price);
            Assert.Equal("Earlybird Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_enter_at_910am_exits_at_1130pm_disqualifies_EarlyBird()
        {
            var startTime = new DateTime(2020, 04, 23, 9, 10, 0);
            var endTime = new DateTime(2020, 04, 23, 23, 0, 0);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Earlybird Rate", rate.Name);
        }

        [Fact]
        public void When_a_car_enter_on_Monday_7am_exits_on_Tuesday_11pm_disqualifies_EarlyBrid()
        {
            var startTime = new DateTime(2020, 04, 20, 7, 0, 0);
            var endTime = new DateTime(2020, 04, 21, 23, 0, 0);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Early Bird", rate.Name);
        }


        [Fact]
        public void When_a_car_exits_after_1130pm_disqualifies_EarlyBird()
        {
            var startTime = new DateTime(2020, 04, 22, 6, 0, 0);
            var endTime = new DateTime(2020, 04, 22, 23, 35, 1);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Early Bird", rate.Name);
        }
    }
}
