﻿using System;
using System.Collections.Generic;
using Domains.Parking;
using Moq;
using Services;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
namespace Tests
{
    public class BaseTest

    {
        protected CalculatingService _calculatingService;
        public const decimal HourlyRate = 5;
        public const decimal EarlyBirdRate = 13;
        public const decimal NightRate = 6.50m;
        public const decimal WeekendRate = 10;
        public const decimal DayRate = HourlyRate * 4;

        public BaseTest()
        {
            var services = new ServiceCollection();
            services.AddTransient(typeof(BaseRule), typeof(EarlyBirdRule));
            services.AddTransient(typeof(BaseRule), typeof(NightRule));
            services.AddTransient(typeof(BaseRule), typeof(StandardRule));
            services.AddTransient(typeof(BaseRule), typeof(WeekendRule));
            var serviceProvider = services.BuildServiceProvider();

            _calculatingService = new CalculatingService(serviceProvider);

        }

        [Fact]
        public void When_entrytime_greaterThan_exitTime_Exception_shouldbe_thrown()
        {
            var startTime = new DateTime(2020, 04, 23, 7, 15, 0);
            var endTime = startTime.AddHours(-2);
            var ex = Assert.Throws<ArgumentException>(() => _calculatingService.Calculate(startTime, endTime));

            Assert.Equal("Bad inputs", ex.Message);

        }



    }
}

