﻿using System;
using Xunit;
namespace Tests
{
    public class WeekendTests: BaseTest
    {
        [Fact]
        public void EnterAfterMidnightFridayAndExitBeforeMidnightSundayIsWeekendRate()
        {
            var startTime = new DateTime(2020, 04, 25, 0, 0, 0);
            var endTime = new DateTime(2020, 04, 25, 23, 59, 59);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.Equal(WeekendRate, rate.Price);           
            Assert.Equal("Weekend Rate", rate.Name);
        }

        [Fact]
        public void WeekendRateNotValidIfStayMoreThan2Days()
        {
            // Start Saturday one week, and leave Sunmday the next week!
            var startTime = new DateTime(2020, 04, 25, 0, 0, 0);
            var endTime = new DateTime(2020, 05, 02, 23, 59, 59);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Weekend Rate", rate.Name);
        }

        [Fact]
        public void StartBeforeMidnightOnFridayDoesNotQualify()
        {
            var startTime = new DateTime(2019, 02, 01, 23, 59, 59);
            var endTime = new DateTime(2019, 02, 03, 23, 59, 59);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Weekend Rate", rate.Name);
        }

        [Fact]
        public void LeavingAfterMidnightOnSundayDoesNotQualify()
        {
            var startTime = new DateTime(2019, 02, 01, 0, 0, 0);
            var endTime = new DateTime(2019, 02, 03, 0, 0, 1);

            var rate = _calculatingService.Calculate(startTime, endTime);
            Assert.NotEqual("Weekend Rate", rate.Name);
        }
    }
}
