﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Domains.Parking;
using FluentValidation;
using MediatR;
using Services.Errors;

namespace Services
{
    public class Calculate
    {
        public class Query : IRequest<FinalRate>
        {
            public DateTime enter { get; set; }
            public DateTime exit { get; set; }

        }

        // Validate enter and exit time
        public class QueryValidator : AbstractValidator<Query>
        {
            public QueryValidator()
            {

                RuleFor(x => x.exit).GreaterThanOrEqualTo(x => x.enter);
            }
        }
        public class Handler : IRequestHandler<Query, FinalRate>
        {
            private readonly ICalculatingService _calculatingService;
            public Handler(ICalculatingService calculatingService)
            {
                _calculatingService = calculatingService;
            }

            public Task<FinalRate> Handle(Query request, CancellationToken cancellationToken)
            {
                FinalRate result;
                try
                {
                    result = _calculatingService.Calculate(request.enter, request.exit);
                } catch (ArgumentException ex)
                {
                    throw new RestException(HttpStatusCode.NotFound, new { Error = ex.Message });
                }
                return Task.FromResult(result);
            }
        }
    }
}
