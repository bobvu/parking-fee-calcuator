﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domains.Parking;
using Microsoft.Extensions.DependencyInjection;

namespace Services
{
    public class CalculatingService : ICalculatingService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly List<BaseRule> rules;
        public CalculatingService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            //get all the rules
            rules = (_serviceProvider.GetServices<BaseRule>()).ToList();
        }

        public FinalRate Calculate(DateTime enter, DateTime exit)
        {
            if (enter >= exit)
            {
                throw new ArgumentException("Bad inputs");
            }

            var ruleResults = new List<FinalRate>();
            foreach (var rule in rules)
            {
                var price = rule.CalculatePrice(enter, exit);
                if (price > 0)
                {
                    ruleResults.Add(new FinalRate(rule.Type.ToString(), rule.Name, price));
                }

            }

            if (ruleResults.Any())
            {
                return (ruleResults.OrderBy(o => o.Price).ToList()).FirstOrDefault();
            }
            else
            {
                throw new Exception("Does not match any rule");
            }
        }
    }
}
