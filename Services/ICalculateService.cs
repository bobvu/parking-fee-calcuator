﻿using System;
using Domains.Parking;

namespace Services
{
    public interface ICalculatingService
    {
        FinalRate Calculate(DateTime entryTime, DateTime exitTime);
    }
}
