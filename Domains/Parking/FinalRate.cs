﻿using System;
namespace Domains.Parking
{
    public class FinalRate
    {
        public FinalRate(string type, string name, decimal price)
        {
            Type = type;
            Name = name;
            Price = price;
        }

        public string Type { get; private set; }
        public string Name { get; private set; }
        public decimal Price { get; private set; }
    }
}
