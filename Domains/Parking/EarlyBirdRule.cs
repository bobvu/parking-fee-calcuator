﻿using System;
namespace Domains.Parking
{
    public class EarlyBirdRule : BaseRule
    {
        
        public override decimal PriceUnit => 13;

        public override RatingType Type => RatingType.FlatRate;

        public override string Name => "Earlybird Rate";

        public override double EntryBegin => 6;

        public override double EntryEnd => 9;

        public override double ExitBegin => 15.5;

        public override double ExitEnd => 23.5;

        public override decimal CalculatePrice(DateTime enter, DateTime exit)
        {
            if (enter.TimeOfDay.TotalHours >= EntryBegin
                                && enter.TimeOfDay.TotalHours <= EntryEnd
                                && exit.TimeOfDay.TotalHours >= ExitBegin
                                && exit.TimeOfDay.TotalHours <= ExitEnd
                                && ((exit.Date - enter.Date).Days == 0))
            {
                return PriceUnit;
            }
            return -1;
        }
    }
}
