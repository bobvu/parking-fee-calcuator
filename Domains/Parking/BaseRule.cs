﻿using System;
namespace Domains.Parking
{
    public abstract class BaseRule
    {
        public abstract decimal PriceUnit { get; }
        public abstract RatingType Type { get; }
        public abstract string Name { get; }
        public abstract double EntryBegin { get; }
        public abstract double EntryEnd { get; }
        public abstract double ExitBegin { get; }
        public abstract double ExitEnd { get; }
        public abstract Decimal CalculatePrice(DateTime enter, DateTime exit);

    }
}
