﻿using System;
namespace Domains.Parking
{
    public class NightRule : BaseRule
    {
        public override RatingType Type { get => RatingType.FlatRate; }
        public override string Name { get => "Night Rate"; }

        public override double EntryBegin => 18;

        public override double EntryEnd => 24;

        public override double ExitBegin => 8;

        public override double ExitEnd => 24;

        public override decimal PriceUnit => 6.50m;


        public override decimal CalculatePrice(DateTime enter, DateTime exit)
        {
            if (enter.TimeOfDay.TotalHours >= EntryBegin
                                && enter.TimeOfDay.TotalHours <= EntryEnd
                                && exit.TimeOfDay.TotalHours < ExitBegin
                              
                                && ((exit - enter).TotalHours <= 24)
                                && enter.DayOfWeek !=DayOfWeek.Saturday
                                && enter.DayOfWeek != DayOfWeek.Sunday)
            {
                return PriceUnit;
            }
            return -1;
        }
    }
}
