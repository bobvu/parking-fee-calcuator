﻿using System;
namespace Domains.Parking
{
    public class WeekendRule : BaseRule
    {
        public override RatingType Type { get => RatingType.FlatRate; }
        public override string Name { get => "Weekend Rate"; }

        public override double EntryBegin => 0;

        public override double EntryEnd => 24;

        public override double ExitBegin => 0;

        public override double ExitEnd => 24;

        public override decimal PriceUnit => 10;

        public override decimal CalculatePrice(DateTime enter, DateTime exit)
        {
            var numberOfDays = exit.Date - enter.Date;
            if ((enter.DayOfWeek == DayOfWeek.Saturday || enter.DayOfWeek == DayOfWeek.Sunday)
                &&
                (exit.DayOfWeek == DayOfWeek.Saturday || exit.DayOfWeek == DayOfWeek.Sunday)
                &&
                numberOfDays.Days <= 2
                )
            {
                return PriceUnit;
            }
            return -1;
        }
    }
}
