﻿using System;
namespace Domains.Parking
{
    public enum RatingType
    {
        FlatRate, HourlyRate
    }
}
