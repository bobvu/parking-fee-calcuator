﻿using System;
namespace Domains.Parking
{
    public class StandardRule : BaseRule
    {
        public override RatingType Type =>  RatingType.HourlyRate;
        public override string Name => "Standard Rate";

        public override double EntryBegin => 0;

        public override double EntryEnd => 24;

        public override double ExitBegin => 0;

        public override double ExitEnd => 24;

        public override decimal PriceUnit => 5;

        public override decimal CalculatePrice(DateTime enter, DateTime exit)
        {
            var duration = exit - enter;
            var numberOfDays = exit.Date - enter.Date;

            if (duration.TotalHours <= 3)
            {
                return PriceUnit * (decimal)Math.Ceiling(duration.TotalHours);

            }
            else
            {
                if (numberOfDays.Days == 0)
                {
                    return PriceUnit * 4;
                } else
                {
                    return 20 * (numberOfDays.Days + 1);
                } 
            }
        }
    }
}
